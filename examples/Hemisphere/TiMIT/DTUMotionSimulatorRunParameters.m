%%
%
% Input parameters for the DTU Motion Simulator.
% This is a low-order floating hemisphere TiMIT case with an irregular wave
% to verify that the response agrees with the original WAMIT RAOs.  
%
%%
dt_sim=0.1; scalef=1.; rho=1024. ; 
FRFtype='TiMIT'; FRFfname='sphere800'; 
GDFfname='sphere800.gdf'; gdfOrder='Low'; 
zeta0file='pmwave.iwf';