%
% Read and plot the geometry in the .xgf file.
%
XGFfname='sphere410.xgf'; ifig=0; ulen=10; grav=9.82;
[ulenx,gravx]=textread(XGFfname,'%f %f %*s',1,'headerlines',1);
if abs(ulen-ulenx)>10^-6 || abs(grav-gravx)>10^-6
    warning('ReadInput:  ulen and/or grav are not consistent between the gdf and the xgf');
    grav
    gravx
    ulen
    ulenx
end
[xbody(1),xbody(2),xbody(3),xbody(4)]=textread(XGFfname,'%f %f %f %f %*s',1,'headerlines',2);
xbody=xbody/ulen;
[isx(1,1),isx(2,1)]=textread(XGFfname,'%d %d %*s',1,'headerlines',3);
[npx]=textread(XGFfname,'%d %*s',1,'headerlines',4);
[xpx,ypx,zpx]=textread(XGFfname,'%f %f %f ','headerlines',5);
xv(1,:,:)=reshape(xpx,4,npx)/ulen; xv(2,:,:)=reshape(ypx,4,npx)/ulen;
xv(3,:,:)=reshape(zpx,4,npx)/ulen;
%
% Work out the centroid, normal vector and area of each panel
%
[xvp,yvp,xct,dir,area,vel]=PanelProperties(xv);
%
%
ifig=ifig+1; figure(ifig); clf; hold on;
for n=1:npx
        plot3(xv(1,:,n),xv(2,:,n),xv(3,:,n),'k')
end
quiver3(xct(1,:),xct(2,:),xct(3,:),vel(1,:),vel(2,:),vel(3,:))
AZ=40;
EL=20;
view(AZ,EL)
xlabel('x')
ylabel('y')
zlabel('z')
axis('equal'); hold off;
%
% 
x=[.1 0 -.1 0 pi/8 0];
xt=x(1:3);
L=RotationMatrix(x(4:6));
