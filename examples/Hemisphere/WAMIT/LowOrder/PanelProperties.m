function [xvp,yvp,xct,dir,area,vel]=PanelProperties(xv)
%
% Compute the panel centroid, area and normal vectors for all the panels 
% listed in xv(1:3,1:4,1:npx).  This is done in the standard WAMIT/TiMIT
% way by passing a plane through the mid-points of each panel edge and 
% dropping the vertices to this plane to create the working flat panel.   
%
nindex=[2,3,4,1]; npx=length(xv(1,1,:));
%
for j=1:3
    xorp(j,:)=(xv(j,1,:)+xv(j,2,:) + xv(j,3,:)+xv(j,4,:))/4;
    ul(j,:)=xv(j,1,:)+xv(j,2,:)-(xv(j,3,:)+xv(j,4,:));
    vl(j,:)=xv(j,1,:)+xv(j,4,:)-(xv(j,2,:)+xv(j,3,:));
end
ulsq(1,:)=1./sqrt(ul(1,:).^2+ul(2,:).^2+ul(3,:).^2);
%
% The direction cosines of the local coordinate system
%
dir(1,1,:)=ul(1,:).*ulsq;
dir(1,2,:)=ul(2,:).*ulsq;
dir(1,3,:)=ul(3,:).*ulsq;
ul(1,:)=reshape(dir(1,2,:),1,npx).*vl(3,:) - reshape(dir(1,3,:),1,npx).*vl(2,:);
ul(2,:)=reshape(dir(1,3,:),1,npx).*vl(1,:) - reshape(dir(1,1,:),1,npx).*vl(3,:);
ul(3,:)=reshape(dir(1,1,:),1,npx).*vl(2,:) - reshape(dir(1,2,:),1,npx).*vl(1,:);
ulsq(1,:)=1./sqrt(ul(1,:).^2+ul(2,:).^2+ul(3,:).^2);
dir(3,1,:)=ul(1,:).*ulsq;
dir(3,2,:)=ul(2,:).*ulsq;
dir(3,3,:)=ul(3,:).*ulsq;
dir(2,1,:)=dir(3,2,:).*dir(1,3,:) - dir(3,3,:).*dir(1,2,:);
dir(2,2,:)=dir(3,3,:).*dir(1,1,:) - dir(3,1,:).*dir(1,3,:);
dir(2,3,:)=dir(3,1,:).*dir(1,2,:) - dir(3,2,:).*dir(1,1,:);
%
% Vertex coordinates in the local system
%
for k=1:4
    vl(1,:)=reshape(xv(1,k,:),1,npx)-xorp(1,:);
    vl(2,:)=reshape(xv(2,k,:),1,npx)-xorp(2,:);
    vl(3,:)=reshape(xv(3,k,:),1,npx)-xorp(3,:);
    xvp(k,:)=reshape(dir(1,1,:),1,npx).*vl(1,:) + ...
        reshape(dir(1,2,:),1,npx).*vl(2,:)+reshape(dir(1,3,:),1,npx).*vl(3,:);
    yvp(k,:)=reshape(dir(2,1,:),1,npx).*vl(1,:) + ...
        reshape(dir(2,2,:),1,npx).*vl(2,:)+reshape(dir(2,3,:),1,npx).*vl(3,:);
end
%
% The panel centroids in the body coordinates.
%
a1=.5*(xvp(1,:)+xvp(2,:)); 
a2=.5*(xvp(1,:)+xvp(4,:)); 
a3=.5*(xvp(1,:)+xvp(3,:)); 
b1=.5*(yvp(1,:)+yvp(2,:)); 
b2=.5*(yvp(1,:)+yvp(4,:)); 
b3=.5*(yvp(1,:)+yvp(3,:)); 
d0=a1.*b2-a2.*b1;
d1=a1.*b3-a3.*b1;
d2=a3.*b2-a2.*b3;
d0i=1./(3*d0);
ul(1,:)=(d1.*a1+d2.*a2).*d0i;
ul(2,:)=(d1.*b1+d2.*b2).*d0i;
%
xct(1,:)=reshape(dir(1,1,:),1,npx).*ul(1,:) + reshape(dir(2,1,:),1,npx).*ul(2,:) + xorp(1,:);
xct(2,:)=reshape(dir(1,2,:),1,npx).*ul(1,:) + reshape(dir(2,2,:),1,npx).*ul(2,:) + xorp(2,:);
xct(3,:)=reshape(dir(1,3,:),1,npx).*ul(1,:) + reshape(dir(2,3,:),1,npx).*ul(2,:) + xorp(3,:);
%
% Vertices
%
for k=1:4
    xvp(k,:)=xvp(k,:) - ul(1,:);    
    yvp(k,:)=yvp(k,:) - ul(2,:);
end
%
% The panel area
%
area=zeros(1,npx);
for n=1:4
    dx=xvp(nindex(n),:)-xvp(n,:);
    dy=yvp(nindex(n),:)-yvp(n,:);
    area=area+.5*dx.*(yvp(n,:)+yvp(nindex(n),:));
end
%
% The generalized unit normal vectors to the panel in the body coordinates.
%
vel(1,:)=dir(3,1,:);
vel(2,:)=dir(3,2,:);
vel(3,:)=dir(3,3,:);
vel(4,:)=xct(2,:).*reshape(dir(3,3,:),1,npx) - xct(3,:).*reshape(dir(3,2,:),1,npx);
vel(5,:)=xct(3,:).*reshape(dir(3,1,:),1,npx) - xct(1,:).*reshape(dir(3,3,:),1,npx);
vel(4,:)=xct(1,:).*reshape(dir(3,2,:),1,npx) - xct(2,:).*reshape(dir(3,1,:),1,npx);
