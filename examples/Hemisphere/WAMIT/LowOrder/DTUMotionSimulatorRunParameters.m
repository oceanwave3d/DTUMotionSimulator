%%
%
% Input parameters for the DTU Motion Simulator.
% This is a low-order floating hemisphere WAMIT case with an irregular wave
% to verify that the response agrees with the original WAMIT RAOs.  
%
%%
dt_sim=0.1; scalef=1.; rho=1024. ; 
FRFtype='WAMIT'; FRFfname='sphere800'; nHead=1; hbot=-1;
newmds=0; imode=[1 0 1 0 0 0];
GDFfname='sphere800.gdf'; gdfOrder='Low'; 
zeta0file='pmwave.iwf';