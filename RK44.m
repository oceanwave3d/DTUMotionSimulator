function [f,df0]=RK44(f0,it0,t0,dt,dfdt)
%
% This is an implementation of the classical explicit 4-stage, 4th-order 
% Runge-Kutta scheme which steps the vector f0(:,1) from t0 to t0+dt
% using the external function df=dfdt(f,t,memory,stage) to give the
% derivatives.  This version is suitable for using a memory function in the
% right hand side and thus passes the pre-computed convolution forces 
% through to the dfdt routine via the variable 'memory'.  The output is 
% f at t0+dt and df/dt at t0.  
%
hdt=0.5*dt; h6=dt/6; th=t0+hdt;
%
% 1st stage (also the derivative at t0):
%
stage=1;
df0=dfdt(f0,it0,t0,stage);
ftem=f0 + hdt*df0;
%
% 2nd stage
%
stage=2;
dftem=dfdt(ftem,it0,th,stage);
ftem=f0 + hdt*dftem;
%
% 3nd stage
%
stage=3;
dfmid=dfdt(ftem,it0,th,stage);
ftem=f0 + dt*dfmid;
dfmid=dftem+dfmid;
%
% 4th stage, compute the new values
%
stage=4;
dftem=dfdt(ftem,it0+1,t0+dt,stage);
f=f0 + h6*(df0+dftem+2*dfmid);
