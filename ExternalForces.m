function F=ExternalForces(t,f,nmodes,stage,flag)
%
% Generic external constraints function.
%
% Input: flag - 0 = initialization call; 1= run time simulation call;
%                   2=post-processing shut-down call
%        t                    - current time
%        f(1:nmodes)          - body velocities in modes 1:nmodes
%        f(nmodes+1:2*nmodes) - body displacement in modes 1:nmodes
%        nmodes               - number of degrees of freedom
%
% Output: F(1:nmodes,1) - External forces applied to each degree of freedom
% 
% If this routine needs to save variables, then declare them here using
% persistent, i.e.: 
%
% persistent U
%
switch flag
    case 0
        %
        % Initialization call for set up.
        %
        display('Dummy external force file, no external forces applied.')
    case 1
        %
        % Run time call
        %
        F=zeros(nmodes,1);
    case 2
        %
        % Final call for post-processing.
        %
        display('Dummy external force file, nothing to do here.')
    otherwise
        error('Called ExternalForces with flag/= 0,1,2')
end
