function [T,A,B]=ReadWAMIT1file(fname,nHead,Order)
%
% A function to read a WAMIT .1 file and return the added mass and damping
% coefficients sorted by frequency and mode.  The returned arrays always 
% have at least 6 modes, so that modes 1-6 correspond to the rigid-body 
% modes.  Depending on Order, the arrays come back with period first or
% modes first.  
%
[Traw,mode1,mode2,Araw,Braw]=textread(fname,'%f %d %d %f %f',-1,'headerlines',nHead);
%
modes=unique(mode1,'stable'); nmodes=length(modes); 
%
T=unique(Traw,'stable'); nf=length(T); ncomb=length(Traw)/nf;
%
switch Order
    case 'TM'
        A=zeros(nf,max(6,nmodes),max(6,nmodes)); B=A;
        %
        for id=1:ncomb
            m(id)=mode1(id); n(id)=mode2(id);
            A(:,m(id),n(id))=Araw(id:ncomb:end);
            B(:,m(id),n(id))=Braw(id:ncomb:end);
        end
    case 'MT'
        A=zeros(max(6,nmodes),max(6,nmodes),nf); B=A;
        %
        for id=1:ncomb
            m(id)=mode1(id); n(id)=mode2(id);
            A(m(id),n(id),:)=Araw(id:ncomb:end);
            B(m(id),n(id),:)=Braw(id:ncomb:end);
        end
    otherwise
        error('Choose either "TM" or "MT" for period or modes first ordering.');
end